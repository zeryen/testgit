package sortbynumber;

import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;


public class SortByNumbersTest {
    @Test
    public void testSortedBySumOfDigits() throws Exception {
        SortByNumbers sorter = new SortByNumbers();
        Assert.assertEquals(Arrays.toString(new String[]{"5", "15", "233", "58"}), sorter.sort(new String[]{"15", "58", "233", "5"}));
    }

    @Test(expected = NumberFormatException.class)
    public void testWrongNumbers() throws Exception {
        SortByNumbers sorter = new SortByNumbers();
        sorter.sort(new String[]{"15", "test", "233", "5"});
    }

    @Test
    public void testNegativeNum() throws Exception {
        SortByNumbers sorter = new SortByNumbers();
        Assert.assertEquals(Arrays.toString(new String[]{"-100", "1", "15", "-25", "25", "7", "233", "58"}), sorter.sort(new String[]{"15", "58", "233", "-25", "7", "-100", "1", "25"}));
    }
}