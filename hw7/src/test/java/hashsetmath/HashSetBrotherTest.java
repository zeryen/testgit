package hashsetmath;

import org.junit.Assert;
import org.junit.Test;
import java.util.*;
import static hashsetmath.HashSetBrother.*;

public class HashSetBrotherTest {
    @Test
    public void testUnion() throws Exception {
        HashSet<Integer> set = union(new HashSet<>(Arrays.asList(7, 8, 9, 5, 2)), new HashSet<>(Arrays.asList(10, 7, 8, 9, 10, 88)));
        Assert.assertEquals(new HashSet<>(Arrays.asList(2, 5, 7, 8, 88, 9, 10)), set);
    }

    @Test
    public void testIntersection() throws Exception {
        HashSet<Integer> set = intersection(new HashSet<>(Arrays.asList(7, 8, 9, 5)), new HashSet<>(Arrays.asList(25, 7, 8, 9, 10, 95)));
        Assert.assertEquals(new HashSet<>(Arrays.asList(7, 8, 9)), set);
    }

    @Test
    public void testDifference() throws Exception {
        HashSet<Integer> set = difference(new HashSet<>(Arrays.asList(7, 8, 9, 5, 2)), new HashSet<>(Arrays.asList(17, 7, 8, 9, 10, 34)));
        Assert.assertEquals(new HashSet<>(Arrays.asList(2, 5)), set);
    }

    @Test
    public void testSymDiff() throws Exception {
        HashSet<Integer> set = symDiff(new HashSet<>(Arrays.asList(7, 8, 9, 5, 2)), new HashSet<>(Arrays.asList(55, 7, 8, 9, 10)));
        Assert.assertEquals(new HashSet<>(Arrays.asList(2, 5, 55, 10)), set);
    }
}