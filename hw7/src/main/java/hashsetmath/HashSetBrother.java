package hashsetmath;

import java.util.*;

public class HashSetBrother {
    public static <T> HashSet<T> union(Set<T> d1, Set<T> d2) {
        HashSet<T> union = new HashSet<>(d1);
        union.addAll(d2);
        return union;
    }

    public static <T> HashSet<T> intersection(Set<T> d1, Set<T> d2) {
        HashSet<T> inter = new HashSet<>(d1);
        inter.retainAll(d2);
        return inter;
    }

    public static <T> HashSet<T> difference(Set<T> d1, Set<T> d2) {
        HashSet<T> diff = new HashSet<T>(d1);
        diff.removeAll(d2);
        return diff;
    }

    public static <T> HashSet<T> symDiff(Set<T> d1, Set<T> d2) {
        HashSet<T> symDiff = new HashSet<>(d1);
        symDiff.addAll(d2);
        HashSet<T> compare = new HashSet<>(d1);
        compare.retainAll(d2);
        symDiff.removeAll(compare);
        return symDiff;
    }
}
