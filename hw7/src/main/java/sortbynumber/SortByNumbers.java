package sortbynumber;

import java.util.*;

public class SortByNumbers {
    private static int sum(String let) {
        int i = 0;
        if (String.valueOf(let.charAt(0)).equals("-")) {
            ++i;
        }
        int res = 0;
        for (; i < let.length(); i++) {
            res += Integer.parseInt("" + let.charAt(i));
        }
        return res;
    }

    public static String sort(String[] string) {
        Arrays.sort(string, new Comparator<String>() {
            public int compare(String lhs, String rhs) {
                int res;
                res = Integer.compare(sum(lhs), sum(rhs));
                return res != 0 ? res : lhs.compareTo(rhs);
            }
        });
        return Arrays.toString(string);
    }

    static public void main(String[] args) {
        try {
            System.out.println(sort(args));
        } catch (NumberFormatException e) {
            System.out.println("Неверное число: " + e.getMessage());
        }
    }
}