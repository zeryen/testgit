package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    private String link = "//a[text()='%s']";
    private final By frame = By.xpath("//iframe[@class='demo-frame']");

    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void openPage(String s) {
        waitForElementVisibility(By.xpath(String.format(link, s)));
        waitForElementIsClickable(By.xpath(String.format(link, s)));
        driver.findElement(By.xpath(String.format(link, s))).click();
    }

    public void switchToFrame() {
        waitForElementVisibility(frame);
        driver.switchTo().frame(driver.findElement(frame));
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(locator));
    }

}
