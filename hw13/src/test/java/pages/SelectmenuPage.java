package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class SelectmenuPage extends Page {
    private final By speedDropDown = By.xpath("//span[@id='speed-button']");
    private final By numberDropDown = By.xpath("//span[@id='number-button']");
    private String value1 = "//li/div[text()='%s']";
    private String value2 = "//span[text()='%s']";

    Actions actions = new Actions(driver);

    public SelectmenuPage(WebDriver driver) {
        super(driver);
    }

    public SelectmenuPage openPage() {
        openPage("Selectmenu");
        switchToFrame();
        return this;
    }

    public SelectmenuPage selectSpeed(String speed) {
        waitForElementIsClickable(speedDropDown);
        setValue(speed, speedDropDown);
        return this;
    }

    public SelectmenuPage selectNumber(String number) {
        waitForElementIsClickable(numberDropDown);
        setValue(number, numberDropDown);
        return this;
    }

    public void setValue(String v, By locator) {
        actions.click(driver.findElement(locator)).build().perform();
        actions.click(driver.findElement(By.xpath(String.format(value1, v)))).build().perform();
    }

    public boolean checkState(String check) {
        return driver.findElement(By.xpath(String.format(value2, check))).isDisplayed();
    }
}
