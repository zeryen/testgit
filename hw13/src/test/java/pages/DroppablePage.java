package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends Page {
    private final By draggable = By.xpath("//div[@id='draggable']");
    private final By droppable = By.xpath("//div[@id='droppable']");
    private final By checkState = By.xpath("//div[contains(@class, 'ui-state-highlight')]");

    Actions actions = new Actions(driver);

    public DroppablePage(WebDriver driver) {
        super(driver);
    }

    public DroppablePage dragAndDrop() {
        openPage("Droppable");
        switchToFrame();
        actions.dragAndDrop(driver.findElement(draggable), driver.findElement(droppable)).build().perform();
        return this;
    }

    public boolean checkState(){
        return driver.findElement(checkState).isDisplayed();
    }

}
