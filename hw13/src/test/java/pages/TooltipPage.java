package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class TooltipPage extends Page {
    private final By tooltip = By.xpath("//a[text()='Tooltips']");
    private final By tooltipCheck = By.xpath("//div[@class='ui-tooltip-content']");

    String actualTitle;
    String expectedTitle;

    Actions actions = new Actions(driver);

    public TooltipPage(WebDriver driver) {
        super(driver);
    }

    public TooltipPage moveMouse() {
        openPage("Tooltip");
        switchToFrame();
        waitForElementVisibility(tooltip);
        expectedTitle = driver.findElement(tooltip).getAttribute("title");
        actions.moveToElement(driver.findElement(tooltip)).build().perform();
        waitForElementVisibility(tooltipCheck);
        actualTitle = driver.findElement(tooltipCheck).getText();
        return this;
    }

    public boolean isDisplayed() {
        return driver.findElement(tooltipCheck).isDisplayed();
    }

    public boolean checkTitle() {
        return expectedTitle.equals(actualTitle);
    }
}
