package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class CheckboxradioPage extends Page {
    private String link = "//label[text()='%s']";

    Actions actions = new Actions(driver);

    public CheckboxradioPage(WebDriver driver) {
        super(driver);
    }

    public CheckboxradioPage openPage() {
        openPage("Checkboxradio");
        switchToFrame();
        return this;
    }

    public CheckboxradioPage checkRadio(String radio) {
        waitForElementIsClickable(By.xpath(String.format(link, radio)));
        actions.click(driver.findElement(By.xpath(String.format(link, radio)))).build().perform();
        return this;
    }

    public CheckboxradioPage checkBox(String box) {
        waitForElementIsClickable(By.xpath(String.format(link, box)));
        actions.click(driver.findElement(By.xpath(String.format(link, box)))).build().perform();
        return this;
    }

    public boolean checkForRadio(String s) {
        return driver.findElement(By.xpath(String.format(link, s))).getAttribute("class").contains("ui-state-active");
    }

    public boolean checkForBox(String s) {
        return driver.findElement(By.xpath(String.format(link, s))).getAttribute("class").contains("ui-state-active");
    }
}
