package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.SelectmenuPage;

public class SelectmenuTest extends Configuration {
    @Test
    public void testSelectmenu() {
        String number = "3";
        String speed = "Fast";
        SelectmenuPage smp = new SelectmenuPage(driver).openPage().selectSpeed(speed).selectNumber(number);
        Assert.assertTrue(smp.checkState(speed));
        Assert.assertTrue(smp.checkState(number));
    }
}
