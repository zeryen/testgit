package tests;


import org.testng.Assert;
import org.testng.annotations.Test;
import pages.DroppablePage;

public class DragAndDropTest extends Configuration {
    @Test
    public void testDragAndDrop() {
        Assert.assertTrue(new DroppablePage(driver).dragAndDrop().checkState());
    }
}
