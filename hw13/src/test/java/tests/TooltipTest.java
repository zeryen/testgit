package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.TooltipPage;

public class TooltipTest extends Configuration {
    @Test
    public void testCheckBoxRadio() {
        TooltipPage ttp = new TooltipPage(driver).moveMouse();
        Assert.assertTrue(ttp.isDisplayed());
        Assert.assertTrue(ttp.checkTitle());
    }
}
