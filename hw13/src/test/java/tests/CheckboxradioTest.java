package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CheckboxradioPage;

public class CheckboxradioTest extends Configuration {
    @Test
    public void testCheckBoxRadio() {
        String radio = "New York";
        String checkBox = "2 Star";
        CheckboxradioPage crp = new CheckboxradioPage(driver).openPage().checkRadio(radio).checkBox(checkBox);
        Assert.assertTrue(crp.checkForRadio(radio));
        Assert.assertTrue(crp.checkForBox(checkBox));
    }
}
