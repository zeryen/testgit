import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class Configuration {
    protected WebDriver driver;
    private static final String MAIL_URL = "https://mail.google.com/mail/";
    public final String email = "test27012018@gmail.com";
    public final String pass = "test123456789";
    public final String address = "test@gmail.com";
    public final String text = "test";
    protected Actions actions;
    int oldDraftSize = 0;
    int newDraftSize = 0;
    int oldSendSize = 0;
    int newSendSize = 0;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(MAIL_URL);
    }

    @AfterMethod
    public void clean() {
        driver.quit();
    }

    protected void waitForElementIsPresent(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForUrl(String string) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(string));
    }

    protected boolean isElementPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void isAlertPresent() {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException Ex) {
            System.out.println("В этот раз без алерта");
        }
    }
}
