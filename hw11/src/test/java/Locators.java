import org.openqa.selenium.By;

public class Locators {
    public static final By emailField = By.xpath("//input[@type='email']");
    public static final By passField = By.cssSelector("input.whsOnd[type='password']");
    public static final By nextButton = By.xpath("//div[@class='O0WRkf zZhnYe e3Duub C0oVfc Zp5qWd Hj2jlf dKVcQ']");
    public static final By inbox = By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3");
    public static final By address = By.cssSelector(".wO.nr.l1");
    public static final By title = By.name("subjectbox");
    public static final By message = By.xpath("//div[@class='Am Al editable LW-avf']");
    public static final By closeButton = By.xpath("//img[@src='images/cleardot.gif' and @class='Ha']");
    public static final By drafts = By.xpath("//a[@href='https://mail.google.com/mail/#drafts']");
    public static final By createdDraft = By.xpath("//tr//span[@class='bog' and text()='test']");
    public static final By sendEmailButton = By.xpath("//tr[@class='btC']/td[1]");
    public static final By sends = By.xpath("//a[@href='https://mail.google.com/mail/#sent']");
    public static final By sendletterConfirm = By.xpath("//div[@class='vh']");
    public static final By accountIcon = By.cssSelector(".gb_Nc.gb_jb.gb_Fg.gb_R");
    public static final By logout = By.xpath("//a[@id='gb_71']");
    public static final By logoutSuccess = By.cssSelector("#forgotPassword");
    public static final By draftLetters = By.xpath("//div[@class='ae4 UI']//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
    public static final By sendLetters = By.xpath("//div[@class='ae4 UI']//table[@class='F cf zt']/tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
}
