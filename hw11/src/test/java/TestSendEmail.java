import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestSendEmail extends Configuration {

    @Test
    public void testSendEmail() throws InterruptedException {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        driver.findElement(Locators.sends).click();
        waitForUrl("sent");
        if (isElementPresent(Locators.sendLetters)) {
            oldSendSize = driver.findElements(Locators.sendLetters).size();
        } else {
            oldSendSize = 0;
        }
        driver.findElement(Locators.inbox).click();
        waitForElementVisibility(Locators.address);
        actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.address));
        actions.click();
        actions.sendKeys(address);
        actions.moveToElement(driver.findElement(Locators.title));
        actions.click();
        actions.click();
        actions.sendKeys(text);
        actions.moveToElement(driver.findElement(Locators.message));
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        driver.findElement(Locators.closeButton).click();
        driver.findElement(Locators.drafts).click();
        waitForUrl("draft");
        driver.navigate().refresh();
        oldDraftSize = driver.findElements(Locators.draftLetters).size();
        driver.findElement(Locators.createdDraft).click();
        waitForElementIsPresent(Locators.sendEmailButton);
        driver.findElement(Locators.sendEmailButton).click();
        waitForElementIsPresent(Locators.sendletterConfirm);
        driver.navigate().refresh();
        isAlertPresent(); //тут иногда всплывает алерт который ломает тест
        if (isElementPresent(Locators.draftLetters)) {
            newDraftSize = driver.findElements(Locators.draftLetters).size();
        } else {
            newDraftSize = 0;
        }
        Assert.assertTrue(oldDraftSize > newDraftSize);
        driver.findElement(Locators.sends).click();
        waitForUrl("sent");
        newSendSize = driver.findElements(Locators.sendLetters).size();
        Assert.assertTrue(newSendSize > oldSendSize);
    }
}
