import org.testng.Assert;
import org.testng.annotations.Test;

public class TestLogout extends Configuration {

    @Test
    public void testLogout() throws InterruptedException {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        driver.findElement(Locators.accountIcon).click();
        driver.findElement(Locators.logout).click();
        waitForElementIsPresent(Locators.logoutSuccess);
        Assert.assertTrue(driver.getCurrentUrl().contains("signin"));
    }
}
