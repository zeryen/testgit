import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCreateEmail extends Configuration {

    @Test
    public void testCreateEmail() throws InterruptedException {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        driver.findElement(Locators.inbox).click();
        waitForElementVisibility(Locators.address);
        actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.address));
        actions.click();
        actions.sendKeys(address);
        actions.moveToElement(driver.findElement(Locators.title));
        actions.click();
        actions.click();
        actions.sendKeys(text);
        actions.moveToElement(driver.findElement(Locators.message));
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        driver.findElement(Locators.closeButton).click();
        driver.findElement(Locators.drafts).click();
        waitForUrl("draft");
        Assert.assertTrue(isElementPresent(Locators.createdDraft));
    }
}
