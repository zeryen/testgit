import org.testng.Assert;
import org.testng.annotations.Test;

public class TestLogin extends Configuration {

    @Test
    public void testLogin() {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        Assert.assertTrue(driver.findElement(Locators.inbox).isDisplayed());
    }
}
