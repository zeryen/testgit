import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestMail extends Configuration {
    public final String email = "test27012018@gmail.com";
    public final String pass = "test123456789";
    public final String address = "test@gmail.com";
    public final String text = "test";
    protected Actions actions;
    int oldDraftSize = 0;
    int newDraftSize = 0;
    int oldSendSize = 0;
    int newSendSize = 0;

    @Test(priority = 1)
    public void testLogin() {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        Assert.assertTrue(driver.findElement(Locators.inbox).isDisplayed());
    }

    @Test(priority = 2)
    public void testCreateEmail() throws InterruptedException {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        driver.findElement(Locators.inbox).click();
        waitForElementVisibility(Locators.address);
        actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.address));
        actions.click();
        actions.sendKeys(address);
        actions.moveToElement(driver.findElement(Locators.title));
        actions.click();
        actions.click();
        actions.sendKeys(text);
        actions.moveToElement(driver.findElement(Locators.message));
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        driver.findElement(Locators.closeButton).click();
        driver.findElement(Locators.drafts).click();
        waitForUrl("draft");
        Assert.assertTrue(isElementPresent(Locators.createdDraft));
    }

    @Test(priority = 3)
    public void testSendEmail() throws InterruptedException {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        driver.findElement(Locators.sends).click();
        waitForUrl("sent");
        if (isElementPresent(Locators.sendLetters)) {
            oldSendSize = driver.findElements(Locators.sendLetters).size();
        } else {
            oldSendSize = 0;
        }
        driver.findElement(Locators.inbox).click();
        waitForElementVisibility(Locators.address);
        actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.address));
        actions.click();
        actions.sendKeys(address);
        actions.moveToElement(driver.findElement(Locators.title));
        actions.click();
        actions.click();
        actions.sendKeys(text);
        actions.moveToElement(driver.findElement(Locators.message));
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        driver.findElement(Locators.closeButton).click();
        driver.findElement(Locators.drafts).click();
        waitForUrl("draft");
        driver.navigate().refresh();
        oldDraftSize = driver.findElements(Locators.draftLetters).size();
        driver.findElement(Locators.createdDraft).click();
        waitForElementIsPresent(Locators.sendEmailButton);
        driver.findElement(Locators.sendEmailButton).click();
        waitForElementIsPresent(Locators.sendletterConfirm);
        driver.navigate().refresh();
        if (isElementPresent(Locators.draftLetters)) {
            newDraftSize = driver.findElements(Locators.draftLetters).size();
        } else {
            newDraftSize = 0;
        }
        Assert.assertTrue(oldDraftSize > newDraftSize);
        driver.findElement(Locators.sends).click();
        waitForUrl("sent");
        newSendSize = driver.findElements(Locators.sendLetters).size();
        Assert.assertTrue(newSendSize > oldSendSize);
    }

    @Test(priority = 4)
    public void testLogout() throws InterruptedException {
        driver.findElement(Locators.emailField).sendKeys(email);
        driver.findElement(Locators.nextButton).click();
        waitForElementVisibility(Locators.passField);
        driver.findElement(Locators.passField).sendKeys(pass);
        waitForElementVisibility(Locators.nextButton);
        driver.findElement(Locators.nextButton).click();
        driver.findElement(Locators.accountIcon).click();
        driver.findElement(Locators.logout).click();
        waitForElementIsPresent(Locators.logoutSuccess);
        Assert.assertTrue(driver.getCurrentUrl().contains("signin"));
    }
}
