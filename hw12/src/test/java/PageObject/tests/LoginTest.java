package PageObject.tests;

import PageObject.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends Configuration {
    @Test
    public void testLogin() {
        Assert.assertTrue(new LoginPage(driver).login().loginCheck());
    }
}
