package PageObject.tests;

import PageObject.pages.CreateEmailPage;
import PageObject.pages.DraftsPage;
import PageObject.pages.LoginPage;
import PageObject.pages.SendsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendEmailTest extends Configuration {
    @Test
    public void testSendEmail() {
        SendsPage sp = new LoginPage(driver).login().openSendsPage();
        int oldSendSize = sp.getNumberOfSendLetters();
        DraftsPage dp = new CreateEmailPage(driver).createDraftLetter().openDraftPage();
        driver.navigate().refresh();
        int oldDraftSize = dp.getNumberOfDraftLetters();
        dp.sendDraftLetter();
        int newDraftSize = dp.getNumberOfDraftLetters();
        Assert.assertTrue(oldDraftSize > newDraftSize);
        int newSendSize = dp.openSendsPage().getNumberOfSendLetters();
        Assert.assertTrue(newSendSize > oldSendSize);
        Assert.assertTrue(sp.checkSendLetter());
    }
}
