package PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    public final String email = "test27012018@gmail.com";
    public final String pass = "test123456789";
    public final String addressEmail = "zeryen@gmail.com";
    public final String text = "test";

    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForUrl(String string) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(string));
    }

    protected void waitForElementIsPresent(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected boolean isElementPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void isAlertPresent() {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException Ex) {
            System.out.println("В этот раз без алерта");
        }
    }
}
