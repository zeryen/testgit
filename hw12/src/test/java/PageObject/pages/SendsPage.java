package PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SendsPage extends MainPage {
    private final By sendLetters = By.xpath("//div[@class='ae4 UI']//table[@class='F cf zt']/tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
    private final By firstSendLetter = By.xpath("//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')][1]");
    private final By checkEmailField = By.xpath("//span[@class='hb']/span");
    private final By checkTitleField = By.xpath("//div[@class='ha']/h2");
    private final By checkMessageField = By.xpath("//div[@class='gs']//div[@dir='ltr']");

    public SendsPage(WebDriver driver) {
        super(driver);
    }

    public int getNumberOfSendLetters() {
        if (isElementPresent(sendLetters)) {
            return driver.findElements(sendLetters).size();
        } else {
            return 0;
        }
    }

    public boolean checkSendLetter(){
        driver.navigate().refresh();
        isAlertPresent();
        driver.findElement(firstSendLetter).click();
        if (driver.findElement(checkEmailField).getAttribute("email").equals(addressEmail)
                && driver.findElement(checkTitleField).getText().equals(text)
                && driver.findElement(checkMessageField).getText().equals(text)) {
            return true;
        } else {
            return false;
        }
    }
}
