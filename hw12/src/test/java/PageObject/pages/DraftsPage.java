package PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends MainPage {
    private final By createdDraft = By.xpath("//tr//span[@class='bog' and text()='test']");
    private final By draftLetters = By.xpath("//div[@class='ae4 UI']//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
    private final By sendletterConfirm = By.xpath("//div[@class='vh']");
    private final By sendEmailButton = By.xpath("//tr[@class='btC']/td[1]");
    private final By checkEmailField = By.xpath("//form/div/div/span[@email]");
    private final By checkTitleField = By.xpath("//input[@name='subject']");
    private final By checkTextBoxField = By.xpath("//div[@role='textbox']");

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkCreatedDraft() {
        driver.findElement(createdDraft).click();
        if (driver.findElement(checkEmailField).getAttribute("email").equals(addressEmail)
                && driver.findElement(checkTitleField).getAttribute("value").equals(text)
                && driver.findElement(checkTextBoxField).getText().equals(text)) {
            return true;
        } else {
            return false;
        }
    }

    public int getNumberOfDraftLetters() {
        if (isElementPresent(draftLetters)) {
            return driver.findElements(draftLetters).size();
        } else {
            return 0;
        }
    }

    public DraftsPage sendDraftLetter() {
        driver.findElement(createdDraft).click();
        waitForElementIsPresent(sendEmailButton);
        driver.findElement(sendEmailButton).click();
        waitForElementIsPresent(sendletterConfirm);
        driver.navigate().refresh();
        isAlertPresent();
        return this;
    }
}
