package PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {
    private final By emailField = By.xpath("//input[@type='email']");
    private final By nextButton = By.xpath("//div[@class='O0WRkf zZhnYe e3Duub C0oVfc Zp5qWd Hj2jlf dKVcQ']");
    private final By passField = By.cssSelector("input.whsOnd[type='password']");
    private final By logoutSuccess = By.cssSelector("#forgotPassword");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public MainPage login() {
        driver.findElement(emailField).sendKeys(email);
        driver.findElement(nextButton).click();
        waitForElementVisibility(passField);
        driver.findElement(passField).sendKeys(pass);
        waitForElementVisibility(nextButton);
        driver.findElement(nextButton).click();
        return new MainPage(driver);
    }

    public boolean logoutCheck(){
        waitForElementIsPresent(logoutSuccess);
        return driver.getCurrentUrl().contains("signin");
    }
}
