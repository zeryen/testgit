package PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends Page {
    private final By inbox = By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3");
    private final By drafts = By.xpath("//a[@href='https://mail.google.com/mail/#drafts']");
    private final By sends = By.xpath("//a[@href='https://mail.google.com/mail/#sent']");
    private final By accountIcon = By.cssSelector(".gb_Nc.gb_jb.gb_Fg.gb_R");
    private final By logoutButton = By.xpath("//a[@id='gb_71']");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public boolean loginCheck() {
        return driver.findElement(inbox).isDisplayed();
    }

    public DraftsPage openDraftPage() {
        driver.findElement(drafts).click();
        waitForUrl("draft");
        return new DraftsPage(driver);
    }

    public SendsPage openSendsPage() {
        driver.findElement(sends).click();
        waitForUrl("sent");
        return new SendsPage(driver);
    }

    public LoginPage logout() {
        driver.findElement(accountIcon).click();
        driver.findElement(logoutButton).click();
        return new LoginPage(driver);
    }
}
