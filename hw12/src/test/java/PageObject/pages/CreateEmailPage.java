package PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateEmailPage extends MainPage {
    private final By inbox = By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3");
    private final By address = By.cssSelector(".wO.nr.l1 textarea");
    private final By title = By.name("subjectbox");
    private final By message = By.xpath("//div[@class='Am Al editable LW-avf']");
    private final By closeButton = By.xpath("//img[@src='images/cleardot.gif' and @class='Ha']");

    public CreateEmailPage(WebDriver driver) {
        super(driver);
    }

    public CreateEmailPage createDraftLetter() {
        driver.findElement(inbox).click();
        waitForElementVisibility(address);
        driver.findElement(address).sendKeys(addressEmail);
        driver.findElement(title).sendKeys(text);
        driver.findElement(message).sendKeys(text);
        driver.findElement(closeButton).click();
        return this;
    }
}
