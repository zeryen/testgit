package PageFactory.pages;

import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public abstract class Page {
    public final String email = "test27012018@gmail.com";
    public final String pass = "test123456789";
    public final String addressEmail = "zeryen@gmail.com";
    public final String text = "test";

    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void waitForElementVisibility(WebElement locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(locator));
    }

    protected void waitForUrl(String string) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(string));
    }

    protected boolean isElementPresent(List<WebElement> locator) {
        return locator.size() > 0;
    }

    public void isAlertPresent() {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException Ex) {
            System.out.println("В этот раз без алерта");
        }
    }
}
