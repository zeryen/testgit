package PageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DraftsPage extends MainPage {
    @FindBy(xpath = "//tr//span[@class='bog' and text()='test']")
    private List<WebElement> createdDraft;

    @FindBy(xpath = "//div[@class='ae4 UI']//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]")
    private List<WebElement> draftLetters;

    @FindBy(xpath = "//div[@class='vh']")
    private WebElement sendletterConfirm;

    @FindBy(xpath = "//tr[@class='btC']/td[1]")
    private WebElement sendEmailButton;

    @FindBy(xpath = "//form/div/div/span[@email]")
    private WebElement checkEmailField;

    @FindBy(xpath = "//input[@name='subject']")
    private WebElement checkTitleField;

    @FindBy(xpath = "//div[@role='textbox']")
    private WebElement checkTextBoxField;

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkCreatedDraft() {
        createdDraft.get(0).click();
        if (checkEmailField.getAttribute("email").equals(addressEmail)
                && checkTitleField.getAttribute("value").equals(text)
                && checkTextBoxField.getText().equals(text)) {
            return true;
        } else {
            return false;
        }
    }

    public int getNumberOfDraftLetters() {
        if (isElementPresent(draftLetters)) {
            return draftLetters.size();
        } else {
            return 0;
        }
    }

    public DraftsPage sendDraftLetter() {
        createdDraft.get(0).click();
        waitForElementVisibility(sendEmailButton);
        sendEmailButton.click();
        waitForElementVisibility(sendletterConfirm);
        driver.navigate().refresh();
        isAlertPresent();
        return this;
    }
}
