package PageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {
    @FindBy(xpath = "//input[@type='email']")
    private WebElement emailField;

    @FindBy(xpath = "//div[@class='O0WRkf zZhnYe e3Duub C0oVfc Zp5qWd Hj2jlf dKVcQ']")
    private WebElement nextButton;

    @FindBy(css = "input.whsOnd[type='password']")
    private WebElement passField;

    @FindBy(css = "#forgotPassword")
    private WebElement logoutSuccess;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public MainPage login() {
        emailField.sendKeys(email);
        nextButton.click();
        waitForElementVisibility(passField);
        passField.sendKeys(pass);
        waitForElementVisibility(nextButton);
        nextButton.click();
        return new MainPage(driver);
    }

    public boolean logoutCheck() {
        waitForElementVisibility(logoutSuccess);
        return driver.getCurrentUrl().contains("signin");
    }
}
