package PageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateEmailPage extends MainPage {
    @FindBy(css = ".T-I.J-J5-Ji.T-I-KE.L3")
    private WebElement inbox;

    @FindBy(css = ".wO.nr.l1 textarea")
    private WebElement address;

    @FindBy(name = "subjectbox")
    private WebElement title;

    @FindBy(xpath = "//div[@class='Am Al editable LW-avf']")
    private WebElement message;

    @FindBy(xpath = "//img[@src='images/cleardot.gif' and @class='Ha']")
    private WebElement closeButton;

    public CreateEmailPage(WebDriver driver) {
        super(driver);
    }

    public CreateEmailPage createDraftLetter() {
        inbox.click();
        waitForElementVisibility(address);
        address.sendKeys(addressEmail);
        title.sendKeys(text);
        message.sendKeys(text);
        closeButton.click();
        return this;
    }
}
