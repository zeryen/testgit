package PageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends Page {
    @FindBy(css = ".T-I.J-J5-Ji.T-I-KE.L3")
    private WebElement inbox;

    @FindBy(xpath = "//a[@href='https://mail.google.com/mail/#drafts']")
    private WebElement drafts;

    @FindBy(xpath = "//a[@href='https://mail.google.com/mail/#sent']")
    private WebElement sends;

    @FindBy(css = ".gb_Nc.gb_jb.gb_Fg.gb_R")
    private WebElement accountIcon;

    @FindBy(xpath = "//a[@id='gb_71']")
    private WebElement logoutButton;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public boolean loginCheck() {
        return inbox.isDisplayed();
    }

    public DraftsPage openDraftPage() {
        drafts.click();
        waitForUrl("draft");
        return new DraftsPage(driver);
    }

    public SendsPage openSendsPage() {
        sends.click();
        waitForUrl("sent");
        return new SendsPage(driver);
    }

    public LoginPage logout() {
        accountIcon.click();
        logoutButton.click();
        return new LoginPage(driver);
    }
}
