package PageFactory.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SendsPage extends MainPage {
    @FindBy(xpath = "//div[@class='ae4 UI']//table[@class='F cf zt']/tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]")
    private List<WebElement> sendLetters;

    @FindBy(xpath = "//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')][1]")
    private WebElement firstSendLetter;

    @FindBy(xpath = "//span[@class='hb']/span")
    private WebElement checkEmailField;

    @FindBy(xpath = "//div[@class='ha']/h2")
    private WebElement checkTitleField;

    @FindBy(xpath = "//div[@class='gs']//div[@dir='ltr']")
    private WebElement checkMessageField;

    public SendsPage(WebDriver driver) {
        super(driver);
    }

    public int getNumberOfSendLetters() {
        if (isElementPresent(sendLetters)) {
            return sendLetters.size();
        } else {
            return 0;
        }
    }

    public boolean checkSendLetter() {
        driver.navigate().refresh();
        isAlertPresent();
        firstSendLetter.click();
        if (checkEmailField.getAttribute("email").equals(addressEmail)
                && checkTitleField.getText().equals(text)
                && checkMessageField.getText().equals(text)) {
            return true;
        } else {
            return false;
        }
    }
}
