package PageFactory.tests;

import PageFactory.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogoutTest extends Configuration {
    @Test
    public void testLogout() {
        Assert.assertTrue(new LoginPage(driver).login().logout().logoutCheck());
    }
}
