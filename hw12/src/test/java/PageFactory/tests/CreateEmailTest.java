package PageFactory.tests;

import PageFactory.pages.CreateEmailPage;
import PageFactory.pages.LoginPage;
import PageFactory.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateEmailTest extends Configuration {
    @Test
    public void testCreateEmail() {
        new LoginPage(driver).login();
        new CreateEmailPage(driver).createDraftLetter();
        Assert.assertTrue(new MainPage(driver).openDraftPage().checkCreatedDraft());
    }
}
