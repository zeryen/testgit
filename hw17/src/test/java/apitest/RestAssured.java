package apitest;

import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.testng.annotations.Test;

import static com.jayway.restassured.RestAssured.*;

public class RestAssured {

    public String TEST_URL = "http://jsonplaceholder.typicode.com/posts";

    @Test
    public void testStatusCode() {
        given().when().get(TEST_URL).then().statusCode(200);
    }

    @Test
    public void testResponseHeader() {
        given().when().get(TEST_URL).then().header("content-type", "application/json; charset=utf-8"). extract().response();
    }

    @Test
    public void testResponseBody() {
        given().when().get(TEST_URL).then().assertThat().body("size()", Matchers.is(100));
    }

    @Test
    public void testPost() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", "foo");
        jsonObject.put("body", "bar");
        jsonObject.put("userID", "1");
        String s = given().body(jsonObject.toString()).when().post(TEST_URL).then().statusCode(201)
                .assertThat().body("isEmpty()", Matchers.is(false))
                .extract().path("id").toString();
        System.out.println(s);
    }

    @Test
    public void testPut() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", "1");
        jsonObject.put("title", "foo");
        jsonObject.put("body", "bar");
        jsonObject.put("userID", "1");
        given().body(jsonObject.toString()).when().put(TEST_URL + "/1").then().statusCode(200)
                .assertThat().body("id", Matchers.is(1));
    }

    @Test
    public void testDelete() {
        given().when().delete(TEST_URL + "/1").then().statusCode(200);
    }
}
