import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

import static jdk.nashorn.internal.objects.Global.Infinity;

public class CalculatorTest {
    Calculator cal;

    @BeforeMethod
    public void setUp() {
        cal = new Calculator();
    }

    @Test(dataProvider = "sumLong", groups = "sum")
    public void testSumLong(long one, long two, long exp) {
        Assert.assertEquals(cal.sum(one, two), exp);
    }

    @Test(dataProvider = "sumDoub", groups = "sum")
    public void testSumDoub(double one, double two, double exp) {
        Assert.assertEquals(cal.sum(one, two), exp);
    }

    @Test(dataProvider = "subLong", groups = "sub")
    public void testSubLong(long one, long two, long exp) {
        Assert.assertEquals(cal.sub(one, two), exp);
    }

    @Test(dataProvider = "subDoub", groups = "sub")
    public void testSubDoub(double one, double two, double exp) {
        Assert.assertEquals(cal.sub(one, two), exp);
    }

    @Test(dataProvider = "mulLong", groups = "mul")
    public void testMulLong(long one, long two, long exp) {
        Assert.assertEquals(cal.mult(one, two), exp);
    }

    @Test(dataProvider = "mulDoub", groups = "mul")
    public void testMulDoub(double one, double two, double exp) {
        Assert.assertEquals(cal.mult(one, two), exp);
    }

    @Test(dataProvider = "divLong", groups = "div")
    public void testDivLong(long one, long two, Object exp) {
        if (exp instanceof Long) {
            Assert.assertEquals(cal.div(one, two), (long) exp);
        }
        else if (exp instanceof Double) {
            Assert.assertEquals(cal.div(one, two), exp);
        }
    }

    @Test(dataProvider = "divDoub", groups = "div")
    public void testDivDoub(double one, double two, Object exp) {
        Assert.assertEquals(cal.div(one, two), exp);
    }

    @Test(dataProvider = "pow", groups = "pow")
    public void testPow(double one, double two, Object exp) {
        Assert.assertEquals(cal.pow(one, two), exp);
    }

    @AfterMethod
    public void cleanUp() {
        cal = null;
    }

    @DataProvider(name = "sumLong")
    public Object[][] getSumL() {
        return new Object[][]{
                {12, 36, 48}, {8, -13, -5}, {9223372036854775807l, 12l, -9223372036854775797l}, {5, 0, 5}
        };
    }

    @DataProvider(name = "sumDoub")
    public Object[][] getSumD() {
        return new Object[][]{
                {10.52, 25.7, 36.22}, {6.58, -87.25, -80.67}, {1.7976931348623, 2.551321, 4.3490141348623}, {2, 0, 2}
        };
    }

    @DataProvider(name = "subLong")
    public Object[][] getSubL() {
        return new Object[][]{
                {85, 13, 72}, {85, 100, -15}, {-9223372036854775808L, 12l, 9223372036854775796l}, {-51, -85, 34}, {39, 0, 39}
        };
    }

    @DataProvider(name = "subDoub")
    public Object[][] getSubD() {
        return new Object[][]{
                {25.52, 10.7, 14.82}, {7.58, 87.25, -79.67}, {1.7976931348623, 2.551321, -0.7536278651377002}, {26, 0, 26}
        };
    }

    @DataProvider(name = "mulLong")
    public Object[][] getMulL() {
        return new Object[][]{
                {85, 13, 1105}, {-9223372036854775801L, 12l, 84}, {-51, -85, 4335}, {39, 0, 0}, {8, -13, -104}, {9223372036854775801l, 12l, -84}
        };
    }

    @DataProvider(name = "mulDoub")
    public Object[][] getMulD() {
        return new Object[][]{
                {25.52, 10.7, 273.064}, {1.79769313486, 2.551321, 4.58649224652415006}, {26, 0, 0}, {-5.5, -5.5, 30.25}, {6.58, -87.25, -575.105}, {1.79769313486, -2.551321, 4.58649224652415006}
        };
    }

    @DataProvider(name = "divLong")
    public Object[][] getDivL() {
        return new Object[][]{
                {100, 10, 10}, {65, 2, 32.5}, {-9223372036854775801L, 10, -922337203685477580.1}, {-51, -85, 0.6}, {39, 0, new NumberFormatException("Attempt to divide by zero")}, {0, 5, 0}, {8, -13, -0.6153846153846154}, {9223372036854775801l, 10, 922337203685477580.1}
        };
    }

    @DataProvider(name = "divDoub")
    public Object[][] getDivD() {
        return new Object[][]{
                {100, 10, 10.0}, {65, 2, 32.5}, {-9223372036854775801L, 10, -922337203685477580.1}, {-51, -85, 0.6}, {39, 0, new NumberFormatException("Attempt to divide by zero")}, {0, 5, 0.0}, {8, -13, -0.6153846153846154}, {9223372036854775801l, 10, 922337203685477580.1}

        };
    }

    @DataProvider(name = "pow")
    public Object[][] getPow(){
        return new Object[][]{
                {2, 2, 4}, {999, 999, Infinity}, {6, 1, 6}, {7, 0, 1}, {-9, 7, -4782969}, {9, -7, 0.00000188167642315892}, {-9, -5, -0.00001693508780843029}, {5.5, 5.5, 11803.064820864423}
        };
    }

}
