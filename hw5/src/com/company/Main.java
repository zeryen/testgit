package com.company;

import java.text.Collator;
import java.util.*;

public class Main {

    public static void print(Set<String> array) {
        char[] alf = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        for (int i = 0; i < alf.length; i++) {
            String s = alf[i] + ": ";
            for (int j = 0; j < array.size(); j++) {
                if (array.toArray()[j].toString().charAt(0) == alf[i]) {
                    s += array.toArray()[j].toString() + " ";
                }
            }
            if (s.length() > 3) {
                System.out.println(s);
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите текст: ");
        String text = in.nextLine();
        text = text.toLowerCase();
        String[] result = text.split("[^a-z]+\\s?");
        Set<String> array = new TreeSet<>(Collator.getInstance());
        for (String s : result) {
            if (s.length() > 0) {
                array.add(s);
            }
        }
        print(array);
    }
}
