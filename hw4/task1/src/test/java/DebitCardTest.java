import org.junit.Test;

import static org.junit.Assert.*;

public class DebitCardTest {
    @Test
    public void testRemoveBallance() throws Exception {
        Card card = new DebitCard("Egor Krid");
        assertEquals(0, card.removeBallance(666), 0);
        Card card1 = new DebitCard("Egor Pop", 100000);
        assertEquals(99000, card1.removeBallance(1000), 0);
    }

}