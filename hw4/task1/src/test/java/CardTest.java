import org.junit.Test;
import static org.junit.Assert.*;

public class CardTest {
    @Test
    public void testGetBallanceConv() throws Exception {
        Card card = new DebitCard("Egor");
        assertEquals(0, card.getBallanceConv(2), 0);
        Card card1 = new DebitCard ("Pasha", 12);
        assertEquals(24, card1.getBallanceConv(2), 0);
        Card card2 = new CreditCard("anton");
        assertEquals(0, card2.getBallanceConv(1.9), 0);
        Card card3 = new CreditCard("anton", 1001);
        assertEquals(2302.2999999999997, card3.getBallanceConv(2.3), 0);
    }

    @Test
    public void testAddBallance() throws Exception {
        Card card = new DebitCard("Egor");
        assertEquals(5800, card.addBallance(5800), 0);
        Card card1 = new DebitCard("Egor", 49);
        assertEquals(826, card1.addBallance(777), 0);
        Card card2 = new CreditCard("anton");
        assertEquals(200, card2.addBallance(200), 0);
        Card card3 = new CreditCard("anton", 1101.2);
        assertEquals(1334.2, card3.addBallance(233), 0);
    }
}