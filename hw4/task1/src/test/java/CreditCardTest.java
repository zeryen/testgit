import org.junit.Test;

import static org.junit.Assert.*;

public class CreditCardTest {
    @Test
    public void testRemoveBallance() throws Exception {
        Card card = new CreditCard("anton");
        assertEquals(-700, card.removeBallance(700), 0);
        Card card1 = new CreditCard("anton", 600);
        assertEquals(-200, card1.removeBallance(800), 0);
    }

}