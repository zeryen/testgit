import org.junit.Test;
import static org.junit.Assert.*;

public class AtmTest {
    @Test
    public void testAddBallanceDebitCard() throws Exception {
        Card card = new DebitCard("anton", 1001.1);
        Atm.addBallance(card, 574.5);
        assertEquals(1575.6, card.getBallance(), 0);
    }

    @Test
    public void testAddBallanceCreditCard() throws Exception {
        Card card = new CreditCard("Anton", 744.44);
        Atm.addBallance(card, 1000);
        assertEquals(1744.44, card.getBallance(), 0);
    }

    @Test
    public void testRemoveBallanceDebitCard() throws Exception {
        Card card = new DebitCard("anton", 2400.2);
        Atm.removeBallance(card, 8100);
        assertEquals(2400.2, card.getBallance(), 0);
    }

    @Test
    public void testRemoveBallanceCreditCard() throws Exception {
        Card card = new CreditCard("anton", 1000);
        Atm.removeBallance(card, 2000);
        assertEquals(-1000, card.getBallance(), 0);
    }

}