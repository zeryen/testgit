public class Atm {

    public static void addBallance(Card card, double amount) {
        card.addBallance(amount);
    }

    public static void removeBallance(Card card, double amount) {
        card.removeBallance(amount);
    }
}
