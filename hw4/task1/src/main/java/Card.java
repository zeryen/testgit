public abstract class Card {
    private String ownerName;
    private double ballance;

    public double setBallance(double ballance) {
        this.ballance = ballance;
        return ballance;
    }

    public double getBallance() {
        return ballance;
    }

    double getBallanceConv(double convertion) {
        return this.ballance * convertion;
    }

    double addBallance(double ballance) {
        return this.ballance += ballance;
    }

    abstract double removeBallance(double ballance);

    Card(String ownerName) {
        this.ownerName = ownerName;
    }

    Card(String ownerName, double ballance) {
        this.ownerName = ownerName;
        this.ballance = ballance;
    }
}
