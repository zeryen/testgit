public class DebitCard extends Card {

    DebitCard(String ownerName) {
        super(ownerName);
    }

    DebitCard(String ownerName, double ballance) {
        super(ownerName, ballance);
    }

    double removeBallance(double ballance) {
        if (getBallance() - ballance < 0) {
            return getBallance();
        } else {
            return setBallance(getBallance() - ballance);
        }
    }
}

