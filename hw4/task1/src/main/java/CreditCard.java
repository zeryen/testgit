public class CreditCard extends Card {

    CreditCard(String ownerName) {
        super(ownerName);
    }

    CreditCard(String ownerName, double ballance) {
        super(ownerName, ballance);
    }

    double removeBallance(double ballance) {
        return setBallance(getBallance() - ballance);
    }
}
