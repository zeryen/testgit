package com.company;

public class Main {
    public static void main(String[] args) {
        int[] array = {2080, 5, 447, 41, 880, 6, 5, 75};
        SortingContext bubble = new SortingContext(new BubbleSort());
        System.out.println("Сортировка Пузырьком:");
        bubble.execute(array);
        SortingContext selection = new SortingContext(new SelectionSort());
        System.out.println("Сортировка Выбором:");
        selection.execute(array);
    }
}
