package com.company;

public interface Sorter {
    String sort(int[] array);
}
