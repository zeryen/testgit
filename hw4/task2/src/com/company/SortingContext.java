package com.company;

public class SortingContext {

    private Sorter sortingStrategy;

    public void execute(int[] array) {
        System.out.println(sortingStrategy.sort(array));
    }

    public SortingContext(Sorter sorter) {
        this.sortingStrategy = sorter;
    }
}

