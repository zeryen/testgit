package com.company;

import java.util.Arrays;

public class SelectionSort implements Sorter {
    @Override
    public String sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < array.length; j++) {

                if (array[j] < array[min])
                    min = j;
            }
            int tmp = array[i];
            array[i] = array[min];
            array[min] = tmp;
        }
        return Arrays.toString(array);
    }
}
