package com.company;

import java.util.Arrays;

public class BubbleSort implements Sorter {
    @Override
    public String sort(int[] array) {
        int n = array.length;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (array[j - 1] > array[j]) {
                    int tmp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = tmp;
                }
            }
        }
        return Arrays.toString(array);
    }
}
