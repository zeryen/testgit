package com.company;

public class Main {
    static int fib = 1;
    static long fac = 1;
    static int i = 1;
    static int a = 0;
    static int b = 1;

    public static void fibo(int loopType, int n) {
        if (loopType == 1) {
            while (i <= n) {
                System.out.print(fib + " ");
                fib = a + b;
                a = b;
                b = fib;
                i++;
            }
        } else if (loopType == 2) {
            if (n == 0) {
                return;
            }
            do {
                System.out.print(fib + " ");
                fib = a + b;
                a = b;
                b = fib;
                i++;
            } while (i <= n);
        } else if (loopType == 3) {
            for (; i <= n; i++) {
                System.out.print(fib + " ");
                fib = a + b;
                a = b;
                b = fib;
            }
        }
    }

    public static void fact(int loopType, int n) {
        if (loopType == 1) {
            while (i <= n) {
                fac = fac * i;
                i++;
            }
        } else if (loopType == 2) {
            do {
                fac = fac * i;
                i++;
            } while (i <= n);
        } else if (loopType == 3) {
            for (; i <= n; i++) {
                fac = fac * i;
            }
        }
        System.out.print("Факториал " + n + " = " + fac);
    }

    public static void main(String[] args) {
        if (args == null || args.length != 3) {
            System.out.println("Неверные значения");
            return;
        }

        int algorithmId = Integer.valueOf(args[0]);
        int loopType = Integer.valueOf(args[1]);
        int n = Integer.valueOf(args[2]);

        if (algorithmId == 1) {
            fibo(loopType, n);
        } else if (algorithmId == 2) {
            fact(loopType, n);
        } else {
            System.out.println("Неверный номер алгоритма");
        }
    }
}

