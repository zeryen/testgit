package com.company;

import static java.lang.Math.*;

public class Main {

    public static void main(String[] args) {
        if (args == null || args.length != 4) {
            System.out.println("Wrong arguments count");
            System.exit(0);
        }

        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);
        double m1 = Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);

        double g = 4 * pow(PI, 2) * ((pow(a, 3)) / (pow(p, 2) * (m1 + m2)));

        System.out.println("Answer: " + g);
    }
}
