package pageobjects.businessobj.message;

public class FirstMessage implements Message {

    private String address = "zeryen@gmail.com";
    private String text = "test";

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public String getText() {
        return text;
    }
}
