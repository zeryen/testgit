package pageobjects.businessobj.message;

public interface MessageCreator {

    public Message create();
}
