package pageobjects.businessobj.message;

public class SecondMessage implements Message {

    private String address = "anton@gmail.com";
    private String text = "anton";

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public String getText() {
        return text;
    }
}
