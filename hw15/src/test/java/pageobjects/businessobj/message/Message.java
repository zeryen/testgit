package pageobjects.businessobj.message;

public interface Message {

    String getAddress();

    String getText();
}
