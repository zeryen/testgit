package pageobjects.businessobj.message;

public class CreateFirstMessage implements MessageCreator {

    @Override
    public Message create() {
        return new FirstMessage();
    }
}
