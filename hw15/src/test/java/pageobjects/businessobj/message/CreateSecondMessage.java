package pageobjects.businessobj.message;

public class CreateSecondMessage implements MessageCreator{

    @Override
    public Message create() {
        return new SecondMessage();
    }
}
