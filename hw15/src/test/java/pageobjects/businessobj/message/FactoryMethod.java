package pageobjects.businessobj.message;

import java.util.Random;

public class FactoryMethod {

    public Message getMessage() {
        Random random = new Random();
        int n = random.nextInt(2) + 1;
        Message message = null;
        MessageCreator mc;
        if (n == 1) {
            mc = new CreateFirstMessage();
            message = mc.create();
        }
        if (n == 2) {
            mc = new CreateSecondMessage();
            message = mc.create();
        }
        return message;
    }
}
