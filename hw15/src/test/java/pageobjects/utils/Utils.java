package pageobjects.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.patterns.Singletone;

public class Utils extends Singletone {

    public static void sendKeys(By value, String message) {
        driver.findElement(value).sendKeys(message);
    }

    public static void click(By value) {
        driver.findElement(value).click();
    }

    public static void refreshPage() {
        driver.navigate().refresh();
    }

    public static boolean isElementPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public static void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void waitForUrl(String string) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(string));
    }

    public static void waitForElementIsPresent(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static void isAlertPresent() {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException Ex) {
            System.out.println("No alert");
        }
    }
}
