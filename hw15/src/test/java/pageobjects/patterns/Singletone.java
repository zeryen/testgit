package pageobjects.patterns;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Singletone {

    public static Decorator driver;

    public Singletone() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            WebDriver dr = new ChromeDriver();
            driver = new Decorator(dr);
        }
        return driver;
    }
}


