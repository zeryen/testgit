package pageobjects.patterns;

import org.openqa.selenium.*;

import java.util.List;
import java.util.Set;

public class Decorator implements WebDriver {

    private WebDriver driver;

    public Decorator(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public void get(String url) {
        System.out.println("Opening " + url);
        driver.get(url);
    }

    @Override
    public String getCurrentUrl() {
        System.out.println("URL is: " + driver.getCurrentUrl());
        return driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        System.out.println("Title is: " + driver.getTitle());
        return driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        try {
            List<WebElement> elems = driver.findElements(by);
            for (WebElement elem : elems) {
                if (driver instanceof JavascriptExecutor) {
                    ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", elem);
                }
            }
            return elems;
        } catch (StaleElementReferenceException ex) {
            System.out.println("Impossible to highlight elements");
            return driver.findElements(by);
        }
    }

    @Override
    public WebElement findElement(By by) {
        try {
            WebElement elem = driver.findElement(by);
            if (driver instanceof JavascriptExecutor) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", elem);
            }
            return elem;
        } catch (StaleElementReferenceException ex) {
            System.out.println("Impossible to highlight element");
            return driver.findElement(by);
        }
    }

    @Override
    public String getPageSource() {
        System.out.println("Page Source is: " + driver.getPageSource());
        return driver.getPageSource();
    }

    @Override
    public void close() {
        System.out.println("Browser tab has been closed");
        driver.close();
    }

    @Override
    public void quit() {
        System.out.println("Browser has been closed");
        driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        for (String handle : driver.getWindowHandles()) {
            System.out.println(handle);
        }
        return driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        System.out.println("Handle is: " + driver.getWindowHandle());
        return driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        System.out.println("Switching to frame");
        return driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        System.out.println("Driver navigate");
        return driver.navigate();
    }

    @Override
    public Options manage() {
        System.out.println("Driver manage");
        return driver.manage();
    }
}
