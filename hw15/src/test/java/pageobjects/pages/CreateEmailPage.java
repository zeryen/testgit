package pageobjects.pages;

import org.openqa.selenium.By;

import static pageobjects.utils.Utils.*;

public class CreateEmailPage extends MainPage {

    private final By inbox = By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3");
    private final By address = By.cssSelector(".wO.nr.l1 textarea");
    private final By title = By.name("subjectbox");
    private final By message = By.xpath("//div[@class='Am Al editable LW-avf']");
    private final By closeButton = By.xpath("//img[@src='images/cleardot.gif' and @class='Ha']");

    public CreateEmailPage openCreateEmailWindow() {
        click(inbox);
        waitForElementVisibility(address);
        return this;
    }

    public CreateEmailPage inputMessage() {
        sendKeys(address, titleAndText.getAddress());
        sendKeys(title, titleAndText.getText());
        sendKeys(message, titleAndText.getText());
        return this;
    }

    public void closeCreateEmailWindow() {
        click(closeButton);
    }
}
