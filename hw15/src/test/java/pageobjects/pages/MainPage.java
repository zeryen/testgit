package pageobjects.pages;

import org.openqa.selenium.By;

import static pageobjects.utils.Utils.*;

public class MainPage extends Page {

    private final By inbox = By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3");
    private final By drafts = By.xpath("//a[@href='https://mail.google.com/mail/#drafts']");
    private final By sends = By.xpath("//a[@href='https://mail.google.com/mail/#sent']");
    private final By accountIcon = By.xpath("//span[@class='gb_ab gbii']");
    private final By logoutButton = By.xpath("//a[@id='gb_71']");

    public boolean loginCheck() {
        return driver.findElement(inbox).isDisplayed();
    }

    public DraftsPage openDraftPage() {
        click(drafts);
        waitForUrl("draft");
        return new DraftsPage();
    }

    public SendsPage openSendsPage() {
        click(sends);
        waitForUrl("sent");
        return new SendsPage();
    }

    public LoginPage logout() {
        click(accountIcon);
        click(logoutButton);
        return new LoginPage();
    }
}
