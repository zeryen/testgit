package pageobjects.pages;

import org.openqa.selenium.By;
import pageobjects.businessobj.user.User;

import static pageobjects.utils.Utils.*;

public class LoginPage extends Page {

    private final By emailField = By.xpath("//input[@type='email']");
    public final By nextPassButton = By.xpath("//div[@id='passwordNext']");
    private final By nextEmailButton = By.xpath("//div[@id='identifierNext']");
    private final By passField = By.cssSelector("input.whsOnd[type='password']");
    private final By logoutSuccess = By.cssSelector("#forgotPassword");

    public boolean logoutCheck() {
        waitForElementIsPresent(logoutSuccess);
        return driver.getCurrentUrl().contains("signin");
    }

    public LoginPage inputEmail() {
        waitForElementVisibility(emailField);
        sendKeys(emailField, User.getEMAIL());
        click(nextEmailButton);
        return this;
    }

    public LoginPage inputPass() {
        waitForElementVisibility(passField);
        sendKeys(passField, User.getPASS());
        click(nextPassButton);
        return this;
    }
}
