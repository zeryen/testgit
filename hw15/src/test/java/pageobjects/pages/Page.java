package pageobjects.pages;

import pageobjects.businessobj.message.FactoryMethod;
import pageobjects.businessobj.message.Message;
import pageobjects.patterns.Singletone;

public abstract class Page extends Singletone {

    static Message titleAndText = new FactoryMethod().getMessage();
}
