package pageobjects.pages;

import org.openqa.selenium.By;

import static pageobjects.utils.Utils.*;

public class DraftsPage extends MainPage {

    private final By createdDraft = By.xpath("//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')][1]");
    private final By draftLetters = By.xpath("//div[@class='ae4 UI']//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
    private final By sendletterConfirm = By.xpath("//div[@class='vh']");
    private final By sendEmailButton = By.xpath("//tr[@class='btC']/td[1]");
    private final By checkEmailField = By.xpath("//form/div/div/span[@email]");
    private final By checkTitleField = By.xpath("//input[@name='subject']");
    private final By checkTextBoxField = By.xpath("//div[@role='textbox']");

    public int getNumberOfDraftLetters() {
        return driver.findElements(draftLetters).size();
    }

    public DraftsPage openFirstDraftLetter() {
        refreshPage();
        isAlertPresent();
        click(createdDraft);
        return this;
    }

    public DraftsPage sendDraftLetter() {
        waitForElementIsPresent(sendEmailButton);
        click(sendEmailButton);
        waitForElementIsPresent(sendletterConfirm);
        return this;
    }

    public boolean checkCreatedDraft() {
        waitForElementVisibility(sendEmailButton);
        return driver.findElement(checkEmailField).getAttribute("email").equals(titleAndText.getAddress())
                && driver.findElement(checkTitleField).getAttribute("value").equals(titleAndText.getText())
                && driver.findElement(checkTextBoxField).getText().equals(titleAndText.getText());
    }
}
