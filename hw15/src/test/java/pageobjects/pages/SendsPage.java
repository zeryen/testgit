package pageobjects.pages;

import org.openqa.selenium.By;

import static pageobjects.utils.Utils.*;

public class SendsPage extends MainPage {

    private final By sendLetters = By.xpath("//div[@class='ae4 UI']//table[@class='F cf zt']/tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
    private final By firstSendLetter = By.xpath("//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')][1]");
    private final By checkEmailField = By.xpath("//span[@class='hb']/span");
    private final By checkTitleField = By.xpath("//div[@class='ha']/h2");
    private final By checkMessageField = By.xpath("//div[@class='gs']//div[@dir='ltr']");

    public int getNumberOfSendLetters() {
        return driver.findElements(sendLetters).size();
    }

    public SendsPage openFirstSendLetter() {
        refreshPage();
        isAlertPresent();
        click(firstSendLetter);
        return this;
    }

    public boolean checkSendLetter() {
        return driver.findElement(checkEmailField).getAttribute("email").equals(titleAndText.getAddress())
                && driver.findElement(checkTitleField).getText().equals(titleAndText.getText())
                && driver.findElement(checkMessageField).getText().equals(titleAndText.getText());
    }
}
