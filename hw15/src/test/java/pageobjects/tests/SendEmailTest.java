package pageobjects.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.pages.*;
import pageobjects.services.Services;

import static pageobjects.utils.Utils.*;

public class SendEmailTest extends Configuration {

    @Test
    public void testSendEmail() {
        Services services = new Services();
        services.login();
        int oldSendSize = services.openSendsPage().getNumberOfSendLetters();
        services.createDraftMessage();
        DraftsPage df = services.openDraftPage();
        int oldDraftSize = df.getNumberOfDraftLetters();
        services.sendFirstDraftLetter();
        refreshPage();
        isAlertPresent();
        int newDraftSize = df.getNumberOfDraftLetters();
        SendsPage sp = services.openSendsPage();
        int newSendSize = sp.getNumberOfSendLetters();
        Assert.assertTrue(sp.openFirstSendLetter().checkSendLetter());
        Assert.assertTrue(oldDraftSize > newDraftSize, "OldDraftsize = " + oldDraftSize + "; NewDraftsize = " + oldDraftSize);
        Assert.assertTrue(newSendSize > oldSendSize, "OldSendsize = " + oldSendSize + "; NewSendsize = " + newSendSize);
    }
}
