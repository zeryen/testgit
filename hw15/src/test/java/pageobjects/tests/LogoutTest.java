package pageobjects.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.pages.*;
import pageobjects.services.Services;

public class LogoutTest extends Configuration {

    @Test
    public void testLogout() {
        Services services = new Services();
        services.login();
        Assert.assertTrue(new MainPage().logout().logoutCheck());
    }
}
