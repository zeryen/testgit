package pageobjects.services;

import pageobjects.pages.CreateEmailPage;
import pageobjects.pages.DraftsPage;
import pageobjects.pages.LoginPage;
import pageobjects.pages.MainPage;

public class Services extends MainPage {

    public void login() {
        LoginPage loginPage = new LoginPage();
        loginPage.inputEmail();
        loginPage.inputPass();
    }

    public void createDraftMessage() {
        CreateEmailPage cep = new CreateEmailPage();
        cep.openCreateEmailWindow();
        cep.inputMessage();
        cep.closeCreateEmailWindow();
    }

    public void sendFirstDraftLetter() {
        DraftsPage dp = new DraftsPage();
        dp.openFirstDraftLetter();
        dp.sendDraftLetter();
    }
}
