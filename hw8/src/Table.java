import java.io.*;
import java.util.*;

public class Table {
    public void addWorker(Map<Integer, Employee> c) {
        System.out.print("Введите имя работника: ");
        Scanner in = new Scanner(System.in);
        String firstName = in.nextLine();
        System.out.print("Введите фамилию работника: ");
        String lastName = in.nextLine();
        c.put(c.size() + 1, new Employee(firstName, lastName));
        save(c);
        System.out.println("Работник успешно добавлен\n");
    }

    public void deleteWorker(Map<Integer, Employee> c) {
        System.out.println("Введите id работника которого вы хотите удалить: ");
        Scanner in = new Scanner(System.in);
        String id = in.nextLine();
        try {
            int i = Integer.parseInt(id);
            if (c.containsKey(i)) {
                for (; i < c.size(); i++) {
                    c.put(i, c.get(i + 1));
                }
                c.remove(i);
                save(c);
                System.out.println("Работник успешно удален\n");
            } else {
                System.out.println("Несуществующий id\n");
            }
        } catch (NumberFormatException e) {
            System.out.println("Неверный id\n");
        }
    }

    public void printWorkers(Map<Integer, Employee> c) {
        if (c.size() > 0) {
            for (Map.Entry<Integer, Employee> entry : c.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
        } else {
            System.out.println("Список пуст");
        }
        System.out.println();
    }

    public static void save(Map<Integer, Employee> c) {
        try {
            FileOutputStream fl = new FileOutputStream("Workers.txt");
            ObjectOutputStream ob = new ObjectOutputStream(fl);
            ob.writeObject(c);
            ob.close();
        } catch (FileNotFoundException e) {
            System.out.println("Файл не был найден " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Файл был поврежден " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        Table table = new Table();
        Map<Integer, Employee> c = new TreeMap<>();

        try {
            FileInputStream fi = new FileInputStream("Workers.txt");
            ObjectInputStream oi = new ObjectInputStream(fi);
            c = (TreeMap<Integer, Employee>) oi.readObject();
            oi.close();
        } catch (FileNotFoundException e) {
            System.out.println("Файл не был найден " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Файл был поврежден " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Файл был поврежден " + e.getMessage());
        }

        while (true) {
            System.out.println("Введите 1 для добавления работника\nВведите 2 для удаления работника\nВведите 3 для того чтобы просмотреть список работников\nВведите 4 для того чтобы выйти");
            Scanner in = new Scanner(System.in);
            String input = in.nextLine();
            if (input.equals("1")) {
                table.addWorker(c);
            } else if (input.equals("2")) {
                table.deleteWorker(c);
            } else if (input.equals("3")) {
                table.printWorkers(c);
            } else if (input.equals("4")) {
                break;
            } else {
                System.out.println("Неверное значение");
            }
        }
    }
}
