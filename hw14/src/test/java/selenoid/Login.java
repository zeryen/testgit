package selenoid;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class Login {

    @Test
    public void login(){
        System.setProperty("selenide.browser", "Chrome");
        open("https://mail.google.com/mail/");
        $(By.xpath("//input[@type='email']")).setValue("test27012018@gmail.com").pressEnter();
        $(By.cssSelector("input.whsOnd[type='password']")).should(visible).setValue("test123456789").pressEnter();
        $(By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3")).shouldBe(visible);
    }
}
