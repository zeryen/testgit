package pageobjects.services;

import org.openqa.selenium.WebDriver;
import pageobjects.pages.*;

import static pageobjects.utils.Utils.click;
import static pageobjects.utils.Utils.waitForElementIsPresent;

public class Services extends MainPage {

    public Services(WebDriver driver) {
        super(driver);
    }

    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.inputEmail();
        loginPage.inputPass();
    }

    public void createDraftMessage() {
        CreateEmailPage cep = new CreateEmailPage(driver);
        cep.openCreateEmailWindow();
        cep.inputMessage();
        cep.closeCreateEmailWindow();
    }

    public void sendFirstDraftLetter() {
        DraftsPage dp = new DraftsPage(driver);
        dp.openFirstDraftLetter();
        waitForElementIsPresent(driver, dp.sendEmailButton);
        click(driver, dp.sendEmailButton);
        waitForElementIsPresent(driver, dp.sendletterConfirm);
    }
}
