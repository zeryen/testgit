package pageobjects.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {

    public static void sendKeys(WebDriver driver, By value, String message) {
        driver.findElement(value).sendKeys(message);
    }

    public static void click(WebDriver driver, By value) {
        driver.findElement(value).click();
    }

    public static void refreshPage(WebDriver driver) {
        driver.navigate().refresh();
    }

    public static boolean isElementPresent(WebDriver driver, By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public static void waitForElementVisibility(WebDriver driver, By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void waitForUrl(WebDriver driver, String string) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(string));
    }

    public static void waitForElementIsPresent(WebDriver driver, By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static void isAlertPresent(WebDriver driver) {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException Ex) {
            System.out.println("No alert");
        }
    }
}
