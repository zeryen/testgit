package pageobjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import pageobjects.businessobj.User;

import static pageobjects.utils.Utils.*;

public class LoginPage extends MainPage {

    public final By emailField = By.xpath("//input[@type='email']");
    public final By nextPassButton = By.xpath("//div[@id='passwordNext']");
    public final By nextEmailButton = By.xpath("//div[@id='identifierNext']");
    public final By passField = By.cssSelector("input.whsOnd[type='password']");
    public final By logoutSuccess = By.cssSelector("#forgotPassword");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage inputEmail(){
        waitForElementVisibility(driver, emailField);
        sendKeys(driver, emailField, User.getEMAIL());
        click(driver, nextEmailButton);
        return this;
    }

    public LoginPage inputPass(){
        waitForElementVisibility(driver, passField);
        sendKeys(driver, passField, User.getPASS());
        click(driver, nextPassButton);
        return this;
    }

    public boolean logoutCheck() {
        waitForElementIsPresent(driver, logoutSuccess);
        return driver.getCurrentUrl().contains("signin");
    }
}
