package pageobjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageobjects.businessobj.Message;

import static pageobjects.utils.Utils.*;

public class SendsPage extends MainPage {

    public final By sendLetters = By.xpath("//div[@class='ae4 UI']//table[@class='F cf zt']/tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
    public final By firstSendLetter = By.xpath("//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')][1]");
    public final By checkEmailField = By.xpath("//span[@class='hb']/span");
    public final By checkTitleField = By.xpath("//div[@class='ha']/h2");
    public final By checkMessageField = By.xpath("//div[@class='gs']//div[@dir='ltr']");

    public SendsPage(WebDriver driver) {
        super(driver);
    }

    public int getNumberOfSendLetters() {
        return driver.findElements(sendLetters).size();
    }

    public SendsPage openFirstSendLetter() {
        click(driver, firstSendLetter);
        return this;
    }

    public boolean checkSendLetter() {
        return driver.findElement(checkEmailField).getAttribute("email").equals(Message.getAddress())
                && driver.findElement(checkTitleField).getText().equals(Message.getText())
                && driver.findElement(checkMessageField).getText().equals(Message.getText());
    }
}
