package pageobjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageobjects.businessobj.*;

import static pageobjects.utils.Utils.*;

public class DraftsPage extends MainPage {

    public final By createdDraft = By.xpath("//tr//span[@class='bog' and text()='test']");
    public final By draftLetters = By.xpath("//div[@class='ae4 UI']//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]");
    public final By sendletterConfirm = By.xpath("//div[@class='vh']");
    public final By sendEmailButton = By.xpath("//tr[@class='btC']/td[1]");
    public final By checkEmailField = By.xpath("//form/div/div/span[@email]");
    public final By checkTitleField = By.xpath("//input[@name='subject']");
    public final By checkTextBoxField = By.xpath("//div[@role='textbox']");

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public DraftsPage openFirstDraftLetter() {
        click(driver, createdDraft);
        return this;
    }

    public boolean checkCreatedDraft() {
        return driver.findElement(checkEmailField).getAttribute("email").equals(Message.getAddress())
                && driver.findElement(checkTitleField).getAttribute("value").equals(Message.getText())
                && driver.findElement(checkTextBoxField).getText().equals(Message.getText());
    }

    public int getNumberOfDraftLetters() {
        return driver.findElements(draftLetters).size();
    }
}
