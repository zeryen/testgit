package pageobjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static pageobjects.utils.Utils.*;

public class MainPage extends Page {

    private final By inbox = By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3");
    private final By drafts = By.xpath("//a[@href='https://mail.google.com/mail/#drafts']");
    private final By sends = By.xpath("//a[@href='https://mail.google.com/mail/#sent']");
    private final By accountIcon = By.xpath("//span[@class='gb_ab gbii']");
    private final By logoutButton = By.xpath("//a[@id='gb_71']");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public boolean loginCheck() {
        return driver.findElement(inbox).isDisplayed();
    }

    public DraftsPage openDraftPage() {
        click(driver, drafts);
        waitForUrl(driver, "draft");
        return new DraftsPage(driver);
    }

    public SendsPage openSendsPage() {
        click(driver, sends);
        waitForUrl(driver, "sent");
        return new SendsPage(driver);
    }

    public LoginPage logout() {
        click(driver, accountIcon);
        click(driver, logoutButton);
        return new LoginPage(driver);
    }
}
