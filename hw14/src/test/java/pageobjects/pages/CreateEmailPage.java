package pageobjects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageobjects.businessobj.Message;

import static pageobjects.utils.Utils.*;

public class CreateEmailPage extends MainPage {

    public final By inbox = By.cssSelector(".T-I.J-J5-Ji.T-I-KE.L3");
    public final By address = By.cssSelector(".wO.nr.l1 textarea");
    public final By title = By.name("subjectbox");
    public final By message = By.xpath("//div[@class='Am Al editable LW-avf']");
    public final By closeButton = By.xpath("//img[@src='images/cleardot.gif' and @class='Ha']");

    public CreateEmailPage(WebDriver driver) {
        super(driver);
    }

    public CreateEmailPage openCreateEmailWindow() {
        click(driver, inbox);
        waitForElementVisibility(driver, address);
        return this;
    }

    public CreateEmailPage inputMessage() {
        sendKeys(driver, address, Message.getAddress());
        sendKeys(driver, title, Message.getText());
        sendKeys(driver, this.message, Message.getText());
        return this;
    }

    public void closeCreateEmailWindow() {
        click(driver, closeButton);
    }
}
