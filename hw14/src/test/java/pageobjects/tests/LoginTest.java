package pageobjects.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.pages.MainPage;
import pageobjects.services.Services;

public class LoginTest extends Configuration {

    @Test
    public void testLogin() {
        Services services = new Services(driver);
        services.login();
        Assert.assertTrue(new MainPage(driver).loginCheck());
    }
}
