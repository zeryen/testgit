package pageobjects.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.pages.DraftsPage;
import pageobjects.pages.SendsPage;
import pageobjects.services.Services;

import static pageobjects.utils.Utils.*;

public class SendEmailTest extends Configuration {

    @Test
    public void testSendEmail() {
        Services services = new Services(driver);
        services.login();
        int oldSendSize = services.openSendsPage().getNumberOfSendLetters();
        services.createDraftMessage();
        DraftsPage df = services.openDraftPage();
        refreshPage(driver);
        int oldDraftSize = df.getNumberOfDraftLetters();
        services.sendFirstDraftLetter();
        refreshPage(driver);
        isAlertPresent(driver);
        int newDraftSize = df.getNumberOfDraftLetters();
        SendsPage sp = services.openSendsPage();
        int newSendSize = sp.getNumberOfSendLetters();
        refreshPage(driver);
        isAlertPresent(driver);
        Assert.assertTrue(sp.openFirstSendLetter().checkSendLetter());
        Assert.assertTrue(oldDraftSize > newDraftSize, "OldDraftsize = " + oldDraftSize + "; NewDraftsize = " + oldDraftSize);
        Assert.assertTrue(newSendSize > oldSendSize, "OldSendsize = " + oldSendSize + "; NewSendsize = " + newSendSize);
    }
}
