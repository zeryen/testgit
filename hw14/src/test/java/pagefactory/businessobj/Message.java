package pagefactory.businessobj;

public class Message {

    private static String address = "zeryen@gmail.com";
    private static String text = "test";

    public static String getAddress() {
        return address;
    }

    public static String getText() {
        return text;
    }
}
