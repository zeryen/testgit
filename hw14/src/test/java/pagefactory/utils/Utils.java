package pagefactory.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Utils {

    public static void refreshPage(WebDriver driver) {
        driver.navigate().refresh();
    }

    public static void waitForElementVisibility(WebDriver driver, WebElement element) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForUrl(WebDriver driver, String string) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(string));
    }

    public static void isAlertPresent(WebDriver driver) {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException Ex) {
            System.out.println("No alert");
        }
    }
}
