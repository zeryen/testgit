package pagefactory.services;

import org.openqa.selenium.WebDriver;
import pagefactory.pages.*;

import static pagefactory.utils.Utils.waitForElementVisibility;

public class Services extends MainPage {

    public Services(WebDriver driver) {
        super(driver);
    }

    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.inputEmail();
        loginPage.inputPass();
    }

    public void createDraftMessage() {
        CreateEmailPage cep = new CreateEmailPage(driver);
        cep.openCreateEmailWindow();
        cep.inputMessage();
        cep.closeCreateEmailWindow();
    }

    public void sendFirstDraftLetter() {
        DraftsPage dp = new DraftsPage(driver);
        dp.openFirstDraftLetter();
        waitForElementVisibility(driver, dp.sendEmailButton);
        dp.sendEmailButton.click();
        waitForElementVisibility(driver, dp.sendletterConfirm);
    }
}
