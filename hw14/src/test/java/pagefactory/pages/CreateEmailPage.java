package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagefactory.businessobj.Message;

import static pagefactory.utils.Utils.waitForElementVisibility;

public class CreateEmailPage extends MainPage {

    @FindBy(css = ".T-I.J-J5-Ji.T-I-KE.L3")
    public WebElement inbox;

    @FindBy(css = ".wO.nr.l1 textarea")
    public WebElement address;

    @FindBy(name = "subjectbox")
    public WebElement title;

    @FindBy(xpath = "//div[@class='Am Al editable LW-avf']")
    public WebElement message;

    @FindBy(xpath = "//img[@src='images/cleardot.gif' and @class='Ha']")
    public WebElement closeButton;

    public CreateEmailPage(WebDriver driver) {
        super(driver);
    }

    public CreateEmailPage openCreateEmailWindow() {
        inbox.click();
        waitForElementVisibility(driver, address);
        return this;
    }

    public CreateEmailPage inputMessage() {
        address.sendKeys(Message.getAddress());
        title.sendKeys(Message.getText());
        message.sendKeys(Message.getText());
        return this;
    }

    public void closeCreateEmailWindow() {
        closeButton.click();
    }
}
