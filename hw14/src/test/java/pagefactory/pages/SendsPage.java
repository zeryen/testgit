package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagefactory.businessobj.*;

import java.util.List;

public class SendsPage extends MainPage {

    @FindBy(xpath = "//div[@class='ae4 UI']//table[@class='F cf zt']/tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]")
    private List<WebElement> sendLetters;

    @FindBy(xpath = "//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')][1]")
    private WebElement firstSendLetter;

    @FindBy(xpath = "//span[@class='hb']/span")
    private WebElement checkEmailField;

    @FindBy(xpath = "//div[@class='ha']/h2")
    private WebElement checkTitleField;

    @FindBy(xpath = "//div[@class='gs']//div[@dir='ltr']")
    private WebElement checkMessageField;

    public SendsPage(WebDriver driver) {
        super(driver);
    }

    public int getNumberOfSendLetters() {
        return sendLetters.size();
    }

    public SendsPage openFirstSendLetter() {
        firstSendLetter.click();
        return this;
    }

    public boolean checkSendLetter() {
        return checkEmailField.getAttribute("email").equals(Message.getAddress())
                && checkTitleField.getText().equals(Message.getText())
                && checkMessageField.getText().equals(Message.getText());
    }
}
