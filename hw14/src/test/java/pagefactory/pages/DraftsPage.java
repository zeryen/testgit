package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagefactory.businessobj.*;

import java.util.List;

public class DraftsPage extends MainPage {

    @FindBy(xpath = "//tr//span[@class='bog' and text()='test']")
    public List<WebElement> createdDraft;

    @FindBy(xpath = "//div[@class='ae4 UI']//tbody/tr[contains(@class, 'zA yO') or contains(@class, 'zA zE')]")
    public List<WebElement> draftLetters;

    @FindBy(xpath = "//div[@class='vh']")
    public WebElement sendletterConfirm;

    @FindBy(xpath = "//tr[@class='btC']/td[1]")
    public WebElement sendEmailButton;

    @FindBy(xpath = "//form/div/div/span[@email]")
    public WebElement checkEmailField;

    @FindBy(xpath = "//input[@name='subject']")
    public WebElement checkTitleField;

    @FindBy(xpath = "//div[@role='textbox']")
    public WebElement checkTextBoxField;

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public DraftsPage openFirstDraftLetter() {
        createdDraft.get(0).click();
        return this;
    }

    public boolean checkCreatedDraft() {
        return checkEmailField.getAttribute("email").equals(Message.getAddress())
                && checkTitleField.getAttribute("value").equals(Message.getText())
                && checkTextBoxField.getText().equals(Message.getText());
    }

    public int getNumberOfDraftLetters() {
        return draftLetters.size();
    }
}
