package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagefactory.businessobj.User;

import static pagefactory.utils.Utils.waitForElementVisibility;

public class LoginPage extends MainPage {

    @FindBy(xpath = "//input[@type='email']")
    public WebElement emailField;

    @FindBy(xpath = "//div[@id='identifierNext']")
    public WebElement nextEmailButton;

    @FindBy(xpath = "//div[@id='passwordNext']")
    public WebElement nextPassButton;

    @FindBy(css = "input.whsOnd[type='password']")
    public WebElement passField;

    @FindBy(css = "#forgotPassword")
    public WebElement logoutSuccess;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage inputEmail() {
        waitForElementVisibility(driver, emailField);
        emailField.sendKeys(User.getEMAIL());
        nextEmailButton.click();
        return this;
    }

    public LoginPage inputPass() {
        waitForElementVisibility(driver, passField);
        passField.sendKeys(User.getPASS());
        nextPassButton.click();
        return this;
    }

    public boolean logoutCheck() {
        waitForElementVisibility(driver, logoutSuccess);
        return driver.getCurrentUrl().contains("signin");
    }
}
