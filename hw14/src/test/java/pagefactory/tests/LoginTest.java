package pagefactory.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pagefactory.pages.MainPage;
import pagefactory.services.Services;

public class LoginTest extends Configuration {

    @Test
    public void testLogin() {
        Services services = new Services(driver);
        services.login();
        Assert.assertTrue(new MainPage(driver).loginCheck());
    }
}
