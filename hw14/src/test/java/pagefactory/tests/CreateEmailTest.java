package pagefactory.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pagefactory.services.Services;

public class CreateEmailTest extends Configuration {

    @Test
    public void testCreateEmail() {
        Services services = new Services(driver);
        services.login();
        services.createDraftMessage();
        Assert.assertTrue(services.openDraftPage().openFirstDraftLetter().checkCreatedDraft());
    }
}
