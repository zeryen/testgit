package pagefactory.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pagefactory.pages.DraftsPage;
import pagefactory.pages.SendsPage;
import pagefactory.services.Services;

import static pagefactory.utils.Utils.isAlertPresent;
import static pagefactory.utils.Utils.refreshPage;

public class SendEmailTest extends Configuration {

    @Test
    public void testSendEmail() {
        Services services = new Services(driver);
        services.login();
        int oldSendSize = services.openSendsPage().getNumberOfSendLetters();
        services.createDraftMessage();
        DraftsPage df = services.openDraftPage();
        refreshPage(driver);
        int oldDraftSize = df.getNumberOfDraftLetters();
        services.sendFirstDraftLetter();
        refreshPage(driver);
        isAlertPresent(driver);
        int newDraftSize = df.getNumberOfDraftLetters();
        SendsPage sp = services.openSendsPage();
        int newSendSize = sp.getNumberOfSendLetters();
        refreshPage(driver);
        isAlertPresent(driver);
        Assert.assertTrue(sp.openFirstSendLetter().checkSendLetter());
        Assert.assertTrue(oldDraftSize > newDraftSize, "OldDraftsize = " + oldDraftSize + "; NewDraftsize = " + oldDraftSize);
        Assert.assertTrue(newSendSize > oldSendSize, "OldSendsize = " + oldSendSize + "; NewSendsize = " + newSendSize);
    }
}
