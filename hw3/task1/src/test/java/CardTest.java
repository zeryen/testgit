import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {
    @Test
    public void testGetBallanceConv() throws Exception {
        Card card = new Card("Egor");
        assertEquals(0, card.getBallanceConv(2), 0);
        Card card1 = new Card ("Pasha", 12.78);
        assertEquals(25.56, card1.getBallanceConv(2), 0);
    }

    @Test
    public void testAddBallance() throws Exception {
        Card card = new Card("Egor");
        assertEquals(5800.79, card.addBallance(5800.79), 0);
        Card card1 = new Card("Egor", 49.7);
        assertEquals(827.47, card1.addBallance(777.77), 0);
    }

    @Test
    public void testRemoveBallance() throws Exception {
        Card card = new Card("Egor Krid");
        assertEquals(-666.6, card.removeBallance(666.6), 0);
        Card card1 = new Card("Egor Pop", 100000);
        assertEquals(99000, card1.removeBallance(1000), 0);
    }

}