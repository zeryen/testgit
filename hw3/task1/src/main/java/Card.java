public class Card {
    private String ownerName;
    private double ballance;

    public double getBallance() {
        return ballance;
    }

    public double getBallanceConv(double conversion) {
        return ballance * conversion;
    }

    public double addBallance(double ballance) {
        return this.ballance += ballance;
    }

    public double removeBallance(double ballance) {
        return this.ballance -= ballance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public Card(String ownerName, double ballance) {
        this.ownerName = ownerName;
        this.ballance = ballance;
    }
}