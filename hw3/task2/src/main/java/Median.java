import java.util.stream.*;

public class Median {

    private static void sort(int[] ints) {
        for (int i = ints.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (ints[j] > ints[j + 1]) {
                    int tmp = ints[j];
                    ints[j] = ints[j + 1];
                    ints[j + 1] = tmp;
                }
            }
        }
    }

    public static float median(int[] ints) {
        sort(ints);
        if (ints.length % 2 == 1) {
            return ints[(ints.length - 1) / 2];
        } else {
            return (ints[(ints.length - 1) / 2] + ints[(ints.length - 1) / 2 + 1]) / 2.0f;
        }
    }

    public static double median(double[] doubles) {
        double m;
        for (int i = doubles.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (doubles[j] > doubles[j + 1]) {
                    double tmp = doubles[j];
                    doubles[j] = doubles[j + 1];
                    doubles[j + 1] = tmp;
                }
            }
        }
        if (doubles.length % 2 == 1) {
            m = doubles[(doubles.length - 1) / 2];
        } else {
            m = (doubles[(doubles.length - 1) / 2] + doubles[(doubles.length - 1) / 2 + 1]) / 2;
        }
        return m;
    }
}

