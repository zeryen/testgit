import java.math.BigDecimal;
import java.util.Comparator;

public class ArrayWrapper<T extends Object> implements Comparator<T> {
    private T[] m;

    public ArrayWrapper(T[] content) {
        this.m = content;
    }

    public T get(int n) {
        if (n <= 0 || n > m.length) {
            throw new IncorrectArrayWrapperIndex();
        } else {
            return m[getIndex(n)];
        }
    }

    public boolean replace(int i, T j) {
        if (i <= 0 || i > m.length) {
            throw new IncorrectArrayWrapperIndex();
        }
        if (j instanceof Number) {
            if (compare(m[getIndex(i)], j) < 0) {
                m[getIndex(i)] = j;
                return true;
            } else {
                return false;
            }
        }
        if (j instanceof String) {
            if ((((String) m[getIndex(i)]).length()) < ((String) j).length()) {
                m[getIndex(i)] = j;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public int getIndex(int n) {
        return n - 1;
    }

    @Override
    public int compare(T a, T b) {
        return new BigDecimal(a.toString()).compareTo(new BigDecimal(b.toString()));
    }
}
