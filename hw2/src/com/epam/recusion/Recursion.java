package com.epam.recusion;

public class Recursion {

    public static int fib(int count) {
        if (count <= 0) {
            return 0;
        } else if (count == 1 || count == 2) {
            return 1;
        } else {
            return fib(count - 1) + fib(count - 2);
        }
    }

    public static long fib2(long count) {
        if (count <= 0) {
            return 0;
        } else if (count == 1 || count == 2) {
            return 1;
        } else {
            return fib2(count - 1) + fib2(count - 2);
        }
    }

    public static void main(String[] args) {
        int countInteger = 0;
        while (fib(countInteger) >= 0) {
            countInteger++;
        }
        System.out.println("Максимальный индекс int " + (countInteger - 1));

        long countLong = 1;
        while (fib2(countLong) >= 0) {
            countLong++;
        }
        System.out.println("Максимальный индекс long " + (countLong - 1));
    }
}
