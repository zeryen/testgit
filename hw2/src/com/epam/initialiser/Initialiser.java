package com.epam.initialiser;

public class Initialiser {
    public static byte a;
    private static short b;
    static char c;
    protected static int d;
    public static long e;
    private static float f;
    protected static double g;
    public static boolean j;
    private static String k;
    private static Integer l;
    public static Initialiser initialiser;
    private static Child child;

    public static void main(String[] args) {
        System.out.println("byte = " + a);
        System.out.println("short = " + b);
        System.out.println("char = " + c);
        System.out.println("int = " + e);
        System.out.println("long = " + f);
        System.out.println("float = " + g);
        System.out.println("double = " + j);
        System.out.println("String = " + k);
        System.out.println("Integer = " + l);
        System.out.println("Initialiser = " + initialiser);
        System.out.println("Child = " + child);
    }
}

