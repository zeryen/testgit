package com.epam.queue;

public class Child extends Parent {
    public static String a = print("Обьявление статической переменной у дочернего класса");
    public String b = print("Обьявление не статической переменной у дочернего класса");

    {
        System.out.println("Блок инициализации у дочернего класса №1");
    }

    static {
        System.out.println("Статический блок инициализации у дочернего класса №1");
    }

    {
        System.out.println("Блок инициализации у дочернего класса №2");
    }

    static {
        System.out.println("Статический блок инициализации у дочернего класса №2");
    }

    public Child() {
        super();
        System.out.println("Присвоение ссылки дочернему обьекту");
    }

}
