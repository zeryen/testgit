package com.epam.queue;

public class Parent {
    public Parent() {
        System.out.println("Присвоение ссылки родительскому обьекту");
    }

    public static String a = print("Обьявление статической переменной у родительского класса");
    public String b = print("Обьявление не статической переменной у родительского класса");

    {
        System.out.println("Блок инициализации у родительского класса №1");
    }

    static {
        System.out.println("Статический блок инициализации у родительского класса №1");
    }

    {
        System.out.println("Блок инициализации у родительского класса №2");
    }

    static {
        System.out.println("Статический блок инициализации у родительского класса №2");
    }

    public static void main(String[] args) {
        Parent parent;
        System.out.println("Создание ссылочкой переменной");
        parent = new Parent();
        parent = new Child();
    }

    public static String print(String print) {
        System.out.println(print);
        return print;
    }
}
