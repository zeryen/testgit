package cucumbertest.services;

import cucumbertest.browser.Browser;
import cucumbertest.pages.MainPage;
import cucumbertest.pages.SearchResultsPage;

public class Services extends Browser {

    public void search(String text) {
        MainPage mainPage = new MainPage(driver);
        mainPage.inputText(text);
        mainPage.clickSearchButton();
    }

    public void addToCart() {
        SearchResultsPage srp = new SearchResultsPage(driver);
        srp.openFirstItem().clickAddToCartButton();
    }
}
