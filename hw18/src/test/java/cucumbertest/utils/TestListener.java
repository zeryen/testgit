package cucumbertest.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import org.junit.runner.Description;
import org.junit.runner.Result;

public class TestListener extends RunListener {

    Logger logger = LogManager.getLogger(this.getClass());

    public void testRunStarted(Description description) throws Exception {
        System.out.println("Number of tests to execute: " + description.testCount());
    }

    public void testRunFinished(Result result) throws Exception {
        System.out.println("Number of tests executed: " + result.getRunCount());
    }

    public void testStarted(Description description) throws Exception {
        System.out.println("Starting: " + description.getMethodName());
    }

    public void testFinished(Description description) throws Exception {
        System.out.println("Finished: " + description.getMethodName());
        logger.info("Test " + description.getMethodName() + " was passed");
        Utils.saveScreenShot("OnPass");
    }

    public void testFailure(Failure failure) throws Exception {
        System.out.println("Failed: " + failure.getDescription().getMethodName());
        logger.error("Test " + failure.getClass() + " was failed");
        Utils.saveScreenShot("OnFail");
    }

    public void testAssumptionFailure(Failure failure) {
        System.out.println("Failed: " + failure.getDescription().getMethodName());
    }

    public void testIgnored(Description description) throws Exception {
        System.out.println("Ignored: " + description.getMethodName());
    }
}
