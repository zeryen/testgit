package cucumbertest.utils;

import cucumber.api.junit.Cucumber;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

import java.io.IOException;

public class TestRunner extends Cucumber {

    public TestRunner(Class clazz) throws InitializationError, IOException {
        super(clazz);
    }

    @Override
    public void run(RunNotifier rN) {
        rN.addListener(new TestListener());
        super.run(rN);
    }
}
