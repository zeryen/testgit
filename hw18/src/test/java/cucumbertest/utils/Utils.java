package cucumbertest.utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static cucumbertest.browser.Browser.driver;

public class Utils {

    public static void saveScreenShot(String fileN) {
        if (driver != null) {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy-h-mm-ss-SS--a");
            String formattedDate = sdf.format(date);
            String fileName = fileN + formattedDate;
            try {
                FileUtils.copyFile(scrFile, new File(String.format("./%s.png", fileName)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
