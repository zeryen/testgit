package cucumbertest.browser;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.concurrent.TimeUnit;

public class Browser {

    Logger logger = LogManager.getLogger(this.getClass());

    public static WebDriver driver;
    private static final String URL = "https://www.ebay.com";

    public void openPage() {
        logger.info("Go to the page " + URL);
        driver.get(URL);
    }

    public void init() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        logger.info("Starting browser " + cap.getBrowserName().toLowerCase() + " " + cap.getPlatform().toString() + " " + cap.getVersion().toString());
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public void closeBrowser() {
        logger.info("Closing browser " + driver);
        driver.quit();
        driver = null;
    }

}
