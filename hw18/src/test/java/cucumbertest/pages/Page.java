package cucumbertest.pages;

import cucumbertest.browser.Browser;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public abstract class Page extends Browser {

    Logger logger = LogManager.getLogger(this.getClass());

    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }
}
