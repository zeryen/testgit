package cucumbertest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage extends Page {

    public final By itemName = By.xpath("//div[@id='ShopCart']//span[@id]/a");

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkItemName(String text) {
        return driver.findElement(itemName).getText().contains(text);
    }
}
