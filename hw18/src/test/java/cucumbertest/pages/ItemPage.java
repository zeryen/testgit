package cucumbertest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ItemPage extends Page {

    public final By addToCartButton = By.xpath("//a[@id='isCartBtn_btn']");

    public ItemPage(WebDriver driver) {
        super(driver);
    }

    public CartPage clickAddToCartButton() {
        logger.debug("Add item to the cart");
        driver.findElement(addToCartButton).click();
        return new CartPage(driver);
    }
}
