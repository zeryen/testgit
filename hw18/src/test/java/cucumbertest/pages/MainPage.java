package cucumbertest.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends Page {

    public final By searchField = By.xpath("//div[@id='gh-ac-box2']/input");
    public final By searchButton = By.xpath("//input[@type='submit']");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public MainPage inputText(String text){
        logger.debug("Enter " + text + " in search field");
        driver.findElement(searchField).sendKeys(text);
        return this;
    }

    public SearchResultsPage clickSearchButton(){
        logger.debug("Click on search button");
        driver.findElement(searchButton).click();
        return new SearchResultsPage(driver);
    }
}
