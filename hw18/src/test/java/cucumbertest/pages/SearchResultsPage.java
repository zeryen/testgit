package cucumbertest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchResultsPage extends Page {

    public final By firstResult = By.xpath("//h3[@class='lvtitle']/a[1]");

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkResult(String text) {
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='5px solid red'", driver.findElement(firstResult));
        }
        return driver.findElement(firstResult).getText().contains(text);
    }

    public ItemPage openFirstItem() {
        logger.debug("Open first link from the list of results");
        driver.findElement(firstResult).click();
        return new ItemPage(driver);
    }
}
