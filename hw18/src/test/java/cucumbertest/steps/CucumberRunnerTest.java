package cucumbertest.steps;

import cucumber.api.CucumberOptions;
import cucumbertest.utils.TestRunner;
import org.junit.runner.RunWith;

@RunWith(TestRunner.class)
@CucumberOptions(
        strict = true,
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }
)
public class CucumberRunnerTest {
}
