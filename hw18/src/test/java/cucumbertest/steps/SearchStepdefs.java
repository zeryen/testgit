package cucumbertest.steps;

import cucumbertest.browser.Browser;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumbertest.pages.CartPage;
import cucumbertest.pages.SearchResultsPage;
import cucumbertest.services.Services;
import cucumber.api.java.Before;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriverException;
import org.testng.Reporter;

import static cucumbertest.utils.Utils.saveScreenShot;
import static org.junit.Assert.assertTrue;

public class SearchStepdefs {

    Logger logger = LogManager.getLogger(this.getClass());

    protected Browser browser = new Browser();

    @Before
    public void start() {
        try {
            browser.init();
        } catch (WebDriverException e) {
            logger.error("Can't initialize browser " + e.getMessage());
            Reporter.log("Error in Web Driver");
        } catch (Exception e) {
            logger.fatal("Can't initialize browser " + e.getMessage());
        }
    }

    @After
    public void clean() {
        try {
            browser.closeBrowser();
        } catch (WebDriverException e) {
            logger.error("Can't close browser " + e.getMessage());
            Reporter.log("Error in Web Driver");
        } catch (Exception e) {
            logger.fatal("Can't close browser " + e.getMessage());
        }
    }

    @Given("^I open eBay page$")
    public void iOpenEBayPage() throws Throwable {
        browser.openPage();
        saveScreenShot("Open Ebay page");
    }

    @When("^I perform \"([^\"]*)\" search$")
    public void iPerformSearch(String arg0) throws Throwable {
        logger.info("Search for " + arg0);
        new Services().search(arg0);
        saveScreenShot("Search");
    }

    @Then("^I see \"([^\"]*)\" in first result$")
    public void iSeeInFirstResult(String arg0) throws Throwable {
        logger.info("Check first result contains " + arg0);
        assertTrue(new SearchResultsPage(browser.driver).checkResult(arg0));
        saveScreenShot("Check result");
    }

    @And("^I open first link and add item to the cart$")
    public void iOpenFirstLinkAndAddItemToTheCart() throws Throwable {
        logger.info("Adding first item to the cart");
        new Services().addToCart();
    }

    @Then("^I see \"([^\"]*)\" in my cart$")
    public void iSeeInMyCart(String arg0) throws Throwable {
        logger.info("Check item " + arg0 + "in the cart");
        assertTrue(new CartPage(browser.driver).checkItemName(arg0));
        saveScreenShot("Check item in cart");
    }
}
