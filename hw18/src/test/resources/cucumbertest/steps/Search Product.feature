Feature: Search for a product

  Scenario: Running search by specific text
    Given I open eBay page
    When I perform "iPhone" search
    Then I see "iPhone" in first result

  Scenario Outline: Running search by different text
    Given I open eBay page
    When I perform "<item>" search
    Then I see "<item>" in first result

    Examples:
      | item      |
      | Philips   |
      | Panasonic |
