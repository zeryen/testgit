Feature: Add To Cart

  Scenario: Running search by specific text
    Given I open eBay page
    When I perform "Intel Core 2 Quad" search
    And I open first link and add item to the cart
    Then I see "Intel Core 2 Quad" in my cart