import java.util.*;
import java.sql.*;

public class DataBase {
    public void addWorker(Connection c) {
        System.out.print("Введите имя работника: ");
        Scanner in = new Scanner(System.in);
        String name = in.nextLine();
        System.out.print("Введите фамилию работника: ");
        String lastName = in.nextLine();

        try {
            PreparedStatement ps = c.prepareStatement("insert into Employee (firstname, lastname) values(?, ?)");
            ps.setString(1, name);
            ps.setString(2, lastName);
            ps.execute();
            ps.close();
            System.out.println("Работник успешно добавлен\n");
        } catch (Exception e) {
            System.out.println("Ошибка добавления: " + e.getStackTrace());
            e.printStackTrace();
        }
    }

    public void deleteWorker(Connection c) {
        System.out.println("Введите id работника которого вы хотите удалить: ");
        Scanner in = new Scanner(System.in);

        try {
            int id = in.nextInt();
            PreparedStatement ps = c.prepareStatement("Delete from Employee where id = ?");
            ps.setInt(1, id);
            ps.execute();
            ps.close();
            System.out.println("Работник успешно удален\n");
        } catch (InputMismatchException e) {
            System.out.println("Неверный id\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printWorkers(Connection c) {
        try {
            Statement s = c.createStatement();
            ResultSet res = s.executeQuery("Select * From Employee");

            while (res.next()) {
                int id = res.getInt("id");
                String firstname = res.getString("firstname");
                String lastname = res.getString("lastname");
                System.out.println("ID = " + id + ": " + firstname + " " + lastname);
            }
            res.close();
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println();
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        DataBase table = new DataBase();
        Class.forName("org.sqlite.JDBC");
        Connection c = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/db_for_m4_l3.db");
        Statement s = c.createStatement();
        s.execute("CREATE TABLE IF NOT EXISTS Employee (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	firstname TEXT,\n"
                + "	lastname TEXT\n"
                + ");");
        s.close();

        while (true) {
            System.out.println("Введите 1 для добавления работника\nВведите 2 для удаления работника\nВведите 3 для того чтобы просмотреть список работников\nВведите 4 для того чтобы выйти");
            Scanner in = new Scanner(System.in);
            String input = in.nextLine();
            if (input.equals("1")) {
                table.addWorker(c);
            } else if (input.equals("2")) {
                table.deleteWorker(c);
            } else if (input.equals("3")) {
                table.printWorkers(c);
            } else if (input.equals("4")) {
                break;
            } else {
                System.out.println("Неверное значение");
            }
        }

        c.close();
    }
}
